

#ifndef RADARFEEDBACKCONTROLLER_H_
#define RADARFEEDBACKCONTROLLER_H_

#include "irisapi/Controller.h"

namespace iris
{

class RadarSystemController: public Controller 
{
private:
  uint32_t counter;

  std::vector<uint8_t> z;

public:
  RadarSystemController();
  virtual void subscribeToEvents();
  virtual void initialize();
  virtual void processEvent(Event &e);
  virtual void destroy();
  void start_receiving(std::vector<uint8_t > &v);
};

} /* namespace iris */

#endif /* RADARFEEDBACKCONTROLLER_H_ */
