

#include "irisapi/LibraryDefs.h"
#include "irisapi/Version.h"
#include "RadarSystemController.h"

using namespace std;
using namespace boost;

namespace iris
{
//Export library functions
IRIS_CONTROLLER_EXPORTS(RadarSystemController);
  
RadarSystemController::RadarSystemController(): Controller("radarsystemcontroller", "A radar system controller ", "Joao F. Santos", "0.1") {
}

// Subscribe to the following events
void RadarSystemController::subscribeToEvents()
{
    // Format: subscribeToEvent(Event name, Component name);
    subscribeToEvent("tx_details", "radartx1");
    subscribeToEvent("pulse_size", "radartx1");
    subscribeToEvent("max_gain", "radartx1");
    subscribeToEvent("min_gain", "radartx1");
}

// Initialize class
void RadarSystemController::initialize() 
{
}

// Process event
void RadarSystemController::processEvent(Event &e) 
{
    Command comInfo;

    //LOG(LINFO) << "EVENT PROCESSED: " << e.eventName << "\t" << e.data.size() << "\t";
    //memcpy(&at_0, &z[0], sizeof(double));
    //memcpy(&at_8, &z[8], sizeof(double));
    //memcpy(&at_16, &z[16], sizeof(unsigned int));
    //LOG(LINFO) << "at_0\t" << at_0 << "\tat_8\t" << at_8 << "\tat_16\t" << at_16;
      
   
    // Receive info about Tx pulse size
    if(e.eventName == "pulse_size") 
    {
      if (z.size() < e.data.size())
          z.resize(e.data.size());

      for(unsigned int i = 0; i < e.data.size(); i++)
        z[i] = boost::any_cast<uint8_t>(e.data[i]);

      unsigned int a;
      memcpy(&a,&z[0],sizeof(unsigned int));

      ReconfigSet r;
      ParametricReconfig p;
      p.engineName = "phyengine2";
      p.componentName = "radarrx1";
      p.parameterName = "pulse_size";
      stringstream str;
      str << a;
      p.parameterValue = str.str();

      r.paramReconfigs.push_back(p);
      reconfigureRadio(r);
    }

    // Receive info about Tx max gain
    if(e.eventName == "max_gain") 
    {
      if (z.size() < e.data.size())
          z.resize(e.data.size());

      for(unsigned int i = 0; i < e.data.size(); i++)
        z[i] = boost::any_cast<uint8_t>(e.data[i]);

      float a;
      memcpy(&a,&z[0],sizeof(float));
      ReconfigSet r;
      ParametricReconfig p;
      p.engineName = "phyengine2";
      p.componentName = "radarrx1";
      p.parameterName = "max_gain";
      stringstream str;
      str << a;
      p.parameterValue = str.str();

      r.paramReconfigs.push_back(p);
      reconfigureRadio(r);
    }

    // Receive info about Tx min gain
    if(e.eventName == "min_gain") 
    {
      if (z.size() < e.data.size())
          z.resize(e.data.size());

      for(unsigned int i = 0; i < e.data.size(); i++)
        z[i] = boost::any_cast<uint8_t>(e.data[i]);

      float a;
      memcpy(&a,&z[0],sizeof(float));
      ReconfigSet r;
      ParametricReconfig p;
      p.engineName = "phyengine2";
      p.componentName = "radarrx1";
      p.parameterName = "min_gain";
      stringstream str;
      str << a;
      p.parameterValue = str.str();

      r.paramReconfigs.push_back(p);
      reconfigureRadio(r);
    }

    // Receive info about Tx pulse time stamps
    if(e.eventName == "tx_details") 
    {
      if (z.size() < e.data.size())
        z.resize(e.data.size());

      for(unsigned int i = 0; i < e.data.size(); i++)
        z[i] = boost::any_cast<uint8_t>(e.data[i]);

      start_receiving(z);
    }
}

// Destructor
void RadarSystemController::destroy() 
{
}

void RadarSystemController::start_receiving(std::vector<uint8_t > &v) 
{
  // Get time stamp
  double a;
  memcpy(&a,&v[4],sizeof(double));

  ReconfigSet r;
  ParametricReconfig p;
  p.engineName = "phyengine2";
  p.componentName = "radarrx1";
  p.parameterName = "controller_data";
  stringstream str;
  str << a;
  p.parameterValue = str.str();

  r.paramReconfigs.push_back(p);
  reconfigureRadio(r);

  // Get instantaneous gain
  float b;
  memcpy(&b,&v[20],sizeof(float));

  ReconfigSet s;
  ParametricReconfig q;
  q.engineName = "phyengine2";
  q.componentName = "radarrx1";
  q.parameterName = "inst_gain";
  stringstream str_;
  str_ << b;
  q.parameterValue = str_.str();

  s.paramReconfigs.push_back(q);
  reconfigureRadio(s);
}

} /* namespace iris */
