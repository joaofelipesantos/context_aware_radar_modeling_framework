# Context-Aware Radar Modeling Framework #
This project provides an experimental framework capable of emulating a wide range of radar systems and recreating their effects on devices sharing its spectrum. The implementation provided here currently interfaces with the Iris software-defined radio system and USRP N210 radio front-end hardware. 

This software code is available for academic use under the GPL license. However, any commercial use will require a full licence. In case of any queries regarding licencing, please, contact the developers