
#include "RadarTx3.h"
#include <sstream>
#include <string>
#include "irisapi/LibraryDefs.h"
#include "irisapi/Version.h"


boost::asio::io_service io_service;
boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), 13417);
boost::asio::ip::tcp::acceptor acceptor(io_service, endpoint);

using namespace std;

namespace iris
{
namespace phy
{

// Export library symbols
IRIS_COMPONENT_EXPORTS(PhyComponent, RadarTx3);

// Register input component parameters
RadarTx3::RadarTx3(std::string name) : PhyComponent(name, "RadarTx3", "Generates radar signals", "Joao F. Santos", "0.1") 
{
  registerParameter("fsampling", "Sampling Frequency", "1e6", true, fsampling_);
  //registerParameter("PTx", "Radar pulse power", "49", true, param_PTx_);
  registerParameter("IPM", "Radar Intra-Pulse Modulation", "LFM", true, ipm_class_);
  registerParameter("PW", "Radar Pulse Duration", "1e-6", true, param_PW_);
  registerParameter("PRI", "Radar Pulse Duration", "1e-3", true, param_PRI_);
  registerParameter("FrequencyExcursion", "Radar LFM frequency excursion", "1e6", true, param_freq_excursion_);
  registerParameter("BarkerLength", "Barker Code Order", "11", true, param_barker_);
  registerParameter("ElevationAngle", "Elevation angle for pencil shape", "0", true, param_elevation_);
  registerParameter("ScanType", "Type of scan pattern", "nothing", true, param_scan_type_);
  registerParameter("ASP", "Antenna scan periodicity [sec]", "4", true, param_ASP_);
  registerParameter("Gmax", "Antenna maximum gain [dB]", "35", true, param_Gmax_);
  registerParameter("ScanSectorCenter", "Center angle for sectorized scan pattern", "0", true, param_angle_range_[0]);
  registerParameter("ScanSectorDiff", "Maximum rotation angle for sectorized scan pattern", "90", true, param_angle_range_[1]);
//  registerParameter("param_rx_angle_", "Direction of the Rx in degrees [0,360]", "30", true, param_rx_angle_);

  registerEvent("tx_details", "Transmission details", TypeInfo<uint8_t>::identifier);
  registerEvent("pulse_size", "Tx pulse size", TypeInfo<uint8_t>::identifier);
  registerEvent("max_gain", "Maximum gain", TypeInfo<uint8_t>::identifier);
  registerEvent("min_gain", "Minimum gain", TypeInfo<uint8_t>::identifier);
}

// Destructor calls destroy method
RadarTx3::~RadarTx3()
{
  destroy();
}

// Register ports
void RadarTx3::registerPorts() 
{
  registerOutputPort("signaloutput1", TypeInfo< Cplx >::identifier);
}

// Calculate output types
void RadarTx3::calculateOutputTypes(std::map<std::string,int>& inputTypes, std::map<std::string,int>& outputTypes) 
{
  outputTypes["signaloutput1"] = TypeInfo< Cplx >::identifier;
}

// Change parameters dynamically and restart radar system
void RadarTx3::parameterHasChanged(std::string name)
{
  if(name == "fsampling" 	|| name == "IPM" 		 || name == "PW"	    || 
     name == "PRI" 	 	|| name == "FrequencyExcursion"  || name == "BarkerLength"  || 
     name == "ElevationAngle"   || name == "ScanType" 		 || name == "ASP"	    ||
     name == "Gmax"  		|| name == "ScanSectorCenter" 	 || name == "ScanSectorDiff"||
     name == "ElevationAngle"   || name == "ScanType" 		 || name == "ScanSectorDiff" )
  {
    destroy();
    initialize();
  }
}

// Initialize the Radar Transmitter 
void RadarTx3::initialize() 
{
    // Converts the IntraPulse Modulation (IPM) string to upper case
    std::transform(ipm_class_.begin(), ipm_class_.end(), ipm_class_.begin(), ::toupper);

    // Tests if the IPM is known
    if(ipm_class_ == "PR") 
    {
	// Creates new Pulse Radar Generator
        radar_ptr = new PulsedRadarGenerator(this);
    }
    else if(ipm_class_ == "LFM") 
    {
	// Creates new Linear Frequency Radar Generator
        radar_ptr = new LFMRadarGenerator(this);//, freq_excursion);
    }
    else if(ipm_class_ == "PM") 
    {
	// Creates new Phase Modulated Radar Generator
        radar_ptr = new PMRadarGenerator(this);//, freq_excursion);
    }  
    else // Throws exception if-non valid IPM is entered
    {
        throw std::invalid_argument( "received invalid IPM: " + ipm_class_ );
    }

    // Check if the entered Barker Code exists
    if ((param_barker_ != 2) && (param_barker_ != 3) && (param_barker_ != 4) && (param_barker_ != 5) && (param_barker_ != 7) && (param_barker_ != 11) && (param_barker_ != 13))
      throw std::invalid_argument( "received an invalid barker code length: " + param_barker_ ); 

    // Check if the pulse size is a multiple of the barker code
    if (  fmod( (float) (fsampling_*param_PW_) , param_barker_  )  )
      throw std::invalid_argument( "received Pulse Width isn't a multiple of the Barker Code: " + (int) param_PW_ ); 

    // Converts the Scan Pattern (SP) string to upper case
    std::transform(param_scan_type_.begin(), param_scan_type_.end(), param_scan_type_.begin(), ::toupper);


    // Tests if the SP is known
    if(param_scan_type_ == "FAN-CIRCULAR")
    {
        LOG(LINFO) << "The radar has a fan-shapped circular scan pattern with ASP of " << param_ASP_;
	// Creates new Circular SP Generator
        esp_gen_ = new FanCircularScanPatternGenerator(this, param_ASP_, param_Gmax_);
    }  

    else if(param_scan_type_ == "FAN-SECTOR") 
    {
        LOG(LINFO) << "The radar has a sectorized scan pattern with ASP of " << param_ASP_;
	// Creates new Sectorial SP Generator	
        esp_gen_ = new FanSectorScanPatternGenerator(this, param_ASP_, param_Gmax_, param_angle_range_);//, param_rx_angle_);
    }
 
    else if(param_scan_type_ == "PENCIL-CIRCULAR")
    {
        LOG(LINFO) << "The radar has a pencil-shapped circular scan pattern with ASP of " << param_ASP_;
	// Creates new Circular SP Generator
        esp_gen_ = new PencilCircularScanPatternGenerator(this, param_ASP_, param_Gmax_, param_elevation_);
    }

    else if(param_scan_type_ == "PENCIL-SECTOR")
    {
        LOG(LINFO) << "The radar has a pencil-shapped sectorized scan pattern with ASP of " << param_ASP_;
	// Creates new Circular SP Generator
        esp_gen_ = new PencilSectorScanPatternGenerator(this, param_ASP_, param_Gmax_, param_angle_range_, param_elevation_);
    } 
    else // There is no radar's SP
    {
        esp_gen_ = NULL;
        param_Gmax_ = 0;
       // Since there is no pattern, the gain is always constant and maximum
    }

    //LOG(LINFO) << "Gain vector size is " << esp_gen_->gain_vector.size();
    LOG(LINFO) << radar_ptr->print();

    // Number of generated samples is the sampling frequency times the time elapsed
    num_samp_gen_ = fsampling_*3.0;  

    radar_ptr->set_PAmp(0.9 / ( (esp_gen_ == NULL) ? 1.0f : pow(10, param_Gmax_ / 20) ));

    // Send pulse size data to controller
    uint8_t * ibegin = reinterpret_cast<uint8_t*>(&radar_ptr->PW_);
    unsigned int size = (sizeof(unsigned int)/sizeof(uint8_t));
    std::vector<uint8_t> temp(ibegin, ibegin + size);
    activateEvent("pulse_size", temp);

    float linear_max_gain = pow(10, esp_gen_->Gmax_ / 20);

    // Send max gain data to controller
    ibegin  = reinterpret_cast<uint8_t*>(&linear_max_gain);
    size = (sizeof(float)/sizeof(uint8_t));
    std::vector<uint8_t> temp_max(ibegin, ibegin + size);
    activateEvent("max_gain", temp_max);

    float linear_min_gain = pow(10,  esp_gen_->Gmin_ / 20);

    // Send min gain data to controller
    ibegin = reinterpret_cast<uint8_t*>(&linear_min_gain);
    size = (sizeof(float)/sizeof(uint8_t));
    std::vector<uint8_t> temp_min(ibegin, ibegin + size);
    activateEvent("min_gain", temp_min);

    old_angle = 0;
    // Creates a thread to do the job
    //comm_thread_.reset(new boost::thread(boost::bind( &RadarTx3::comm_thread, this)));
}

void RadarTx3::destroy()
{
  delete radar_ptr;
  delete esp_gen_;
}


// Process
void RadarTx3::process() 
{	
    //boost::posix_time::ptime initial_clock_ = boost::posix_time::microsec_clock::local_time();       

    //	boost::posix_time::ptime initial_clock_ = boost::posix_time::microsec_clock::local_time(); 

    // Initialize the output DataSet
    DataSet<Cplx>* writeDataSet = NULL;
    std::size_t pulse_size = (size_t)radar_ptr->get_pulse_duration();
    unsigned int period_size = (unsigned int) radar_ptr->get_period_duration();
    unsigned int scale = esp_gen_->scale_coeff;

        getOutputDataSet("signaloutput1", writeDataSet, pulse_size);

        // Start the roation at a random phase
	//int inner_it = (int) (rand() % esp_gen_->gain_vector.size()); 

        // Fills DataSet starting from the back
        vector<Cplx>::iterator output_dataset_iterator = writeDataSet->data.begin();

        // While inside the PW, generate pulse, but after it, keep increasing the gain vector until the next pulse
      float gain = esp_gen_->get_gain_sample(period_size);

      float angle =  esp_gen_->get_radar_angle();
      
      if ((angle > old_angle+1))
      {
        old_angle = angle;
        boost::asio::ip::tcp::iostream stream;
        boost::system::error_code ec;
        acceptor.accept(*stream.rdbuf(), ec);
      
        if (!ec)
        {
          stream << angle;
        }
      }
        do
        {
          // This is a reverse itarator, but the fill order is maintained	 
	  for(register int i = scale; i >= 0; i--)
          {  
            (*output_dataset_iterator) = (radar_ptr->get_waveform_sample() * gain);
            output_dataset_iterator++;

            if (output_dataset_iterator == writeDataSet->data.end()) 
              break;
          }

        }while( output_dataset_iterator != writeDataSet->data.end());

        writeDataSet->sampleRate = fsampling_;
        writeDataSet->timeStamp = (double) (((double) num_samp_gen_) / (double) fsampling_);
        num_samp_gen_ += radar_ptr->PRI_;

        double tstamp[2];
        tstamp[0] = (double) ((double) num_samp_gen_) / (double) fsampling_;
        tstamp[1] = (double) ((double) (num_samp_gen_ + pulse_size)) / (double) fsampling_;

        releaseOutputDataSet("signaloutput1", writeDataSet);
        std::vector<uint8_t> controller_buffer = rx_controller.data(tstamp,gain);
        activateEvent("tx_details", controller_buffer);

}


/* ************************************************************************** */
/* ************************ RadarSigGenerator Class ************************* */
/* ************************************************************************** */

RadarSigGenerator::RadarSigGenerator(RadarTx3 *radarsys, IPM_TYPE ipm) : radarsys_(*radarsys) 
{
  IPM_ = ipm;

  PW_ = (TOA_FORMAT) round(radarsys_.param_PW_ * radarsys_.fsampling_);
  PRI_ = (TOA_FORMAT) round(radarsys_.param_PRI_ * radarsys_.fsampling_);

  temp_PRI_ = PRI_;
  //gen_clock_ = (rand() % temp_PRI_);  // start with a random phase!
  gen_clock_ = 0;
}

// Generates radar samples
inline Cplx RadarSigGenerator::get_waveform_sample() 
{
  // If clock is within the pulse duration, it returns the pulse waveform. Otherwise the output is zeroed

  //LOG(LINFO) << "Clock " << gen_clock_  << "\t" << pulse_waveform_[gen_clock_];
  Cplx waveform = pulse_waveform_[gen_clock_];

  gen_clock_++;

  if (gen_clock_ >= PW_)
    gen_clock_ = 0;

  return waveform;
}

/* ************************************************************************** */
/* ************************ Intrapulse Modulations  ************************* */
/* ************************************************************************** */

// Simples Pulse
PulsedRadarGenerator::PulsedRadarGenerator(RadarTx3 *gen) : RadarSigGenerator(gen, IPM_PR) 
{
  generate_pulse();
}

// Simple Pulse Generation
void PulsedRadarGenerator::generate_pulse() 
{
  pulse_waveform_.assign(PW_, Cplx(1,0));
}

// Linear Frequency Modulation
LFMRadarGenerator::LFMRadarGenerator(RadarTx3 *gen) : RadarSigGenerator(gen, IPM_LFM) 
{
  freq_excursion_ = radarsys_.param_freq_excursion_ / radarsys_.fsampling_;
  generate_pulse();
}

// LInear Frequency Modulatin Pulse Generation
void LFMRadarGenerator::generate_pulse() 
{
  pulse_waveform_.resize(PW_);
  float ramp;	

  for(register unsigned int t = 0; t < PW_ ; t++) 
  {
    ramp = 2*M_PI*(freq_excursion_*(t*t)/(2*(float)PW_) - 0.5*freq_excursion_*t);
    pulse_waveform_[t] = Cplx(1,0)*std::exp(Cplx(0, ramp));
    //LOG(LINFO) << t << ": " << ramp << " " << pulse_waveform_[t];	
  }
}

// Phase Modulation
PMRadarGenerator::PMRadarGenerator(RadarTx3 *gen) : RadarSigGenerator(gen, IPM_PM) 
{
  barker_order_ = radarsys_.param_barker_;
  generate_pulse();
}

// Phase Modulation Pulse Generation
void PMRadarGenerator::generate_pulse() 
{ 
  // Check if pulse width have the right size as a multiple of the barber length  
  if (   PW_ % barker_order_  )
    throw std::invalid_argument( "received Pulse Width isn't a multiple of the Barker Code: " + barker_order_ );  

  // Barker Codes
  const int barker_2[2] = {1, -1};
  const int barker_3[3] = {1, 1, -1};
  const int barker_4[4] = {1, 1, -1, 1};
  const int barker_5[5] = {1, 1, 1, -1, 1};
  const int barker_7[7] = {1, 1, 1, -1, -1, 1, -1};
  const int barker_11[11] = {1, 1, 1, -1, -1, -1, 1, -1, -1, 1, -1};
  const int barker_13[13] = {1, 1, 1, 1, 1, -1, -1, 1, 1, -1, 1, -1, 1};

  // Assign pointer to the right barker code
  switch(barker_order_)
  {
    case 2:
      barker_mask = barker_2;
      break;

    case 3:
      barker_mask = barker_3;
      break;

    case 4:
      barker_mask = barker_4;
      break;

    case 5:
      barker_mask = barker_5;
      break;

    case 7:
      barker_mask = barker_7;
      break;

    case 11:
      barker_mask = barker_11;
      break;

    case 13:
      barker_mask = barker_13;
      break;
  }

  // Resize waveform to pulse size
  pulse_waveform_.resize(PW_);

  unsigned int index = barker_order_-1;
  int t = PW_-1;
  
  // Generates Barker Code Phase Modulation Waveform
  while (t >= 0)
  { 
    for ( int inner_it = (int) (PW_/barker_order_); inner_it != 0; inner_it--)
    {
      pulse_waveform_[t] = Cplx((*(barker_mask+index)),0);

      //LOG(LINFO) << t << ": \t" << pulse_waveform_[t] << "\tindex " << index << " inner " << inner_it;

      t--;
    }

    index--;
  }
   
}

// Output Radar Type
string RadarSigGenerator::print() const 
{
  ostringstream ostr; 
  
  ostr << "Printing Radar Type..." << endl << "IPM: ";
  if(IPM_ == IPM_PR) 
  {
    ostr << "PR";
  }

  else if(IPM_ == IPM_LFM) 
  {
    ostr << "LFM";
  }

  else if(IPM_ == IPM_PM) 
  {
    ostr << "PM";
  }

  else if(IPM_ == IPM_NLFM) 
  {
    ostr << "NLFM";
  }


  ostr << endl << "PW = " << PW_  << endl << "PRI = " << PRI_ << endl; 
  return ostr.str();
}

void RadarSigGenerator::set_PAmp(float val) {
    PAmp_ = val;

    float max_el = 0;
    for(register int i = pulse_waveform_.size(); i >= 0; i--)
        if(std::abs(pulse_waveform_[i]) > max_el) max_el = std::abs(pulse_waveform_[i]);

    for(register int i = pulse_waveform_.size(); i >= 0; i--)
        pulse_waveform_[i] = pulse_waveform_[i] * PAmp_ / max_el;
}

/* ************************************************************************** */
/* ********************** ScanPatternGenerator Class ************************ */
/* ************************************************************************** */

ScanPatternGenerator::ScanPatternGenerator(RadarTx3 *radarsys, float ASP, float Gmax) : radarsys_(*radarsys)
{
  ASP_ = round(ASP*radarsys_.fsampling_);
  Gmax_ = Gmax;

  // Calclate the angles for the different gain regions
  theta_.resize(3);
  theta_[0] = 50*pow(0.25f*Gmax_ + 7, 0.5f) * pow(10, -0.05f *Gmax_ );
  
  if(Gmax_ > 48) 
  { // Very high gain
    gain_type_ = 1;
    theta_[1] = 27.466f * pow(10, -0.03f * Gmax_);
    theta_[2] = 48;
    Gmin_ = -13;
  }
 
  else if(Gmax_ > 22) 
  {
    gain_type_ = 2;
    theta_[1] = 250 * pow(10, -0.05f * Gmax_);
    theta_[2] = 48;
    Gmin_ = 11 - 0.5f *Gmax_;
  } 

  else if(Gmax_ > 10) 
  {
    gain_type_ = 3;
    theta_[1] = 250 * pow(10, -0.05f * Gmax_);
    theta_[2] = 131.8257f * pow(10, -0.02f * Gmax_);
    Gmin_ = 0;
  } 

  else 
  {
    throw invalid_argument("The Antenna gain is too low");
  }

  // Phase step for each sample
  phase_step_ = (360.0) / ASP_; 
  scale_coeff = 100;
  base_gain.resize(ASP_/(2*scale_coeff));
  base_gain_angle.resize(ASP_/(2*scale_coeff));
  gain_it = 0;
  
  float less_precision_phase_step = scale_coeff * phase_step_;

  //LOG(LINFO) << "Phase Step " << phase_step_ << "\tLess Precicion Phase Step " << less_precision_phase_step ;
  register unsigned int i = 0;
  for(register float angle = 0.0f; angle < 180; angle+= less_precision_phase_step)
  {
    base_gain[i] = radiation_pattern_gain(angle);
    base_gain_angle[i] = angle;
    i++;
  }
  // Pad end of base gain vector with the last calculated value
  double last_value_calculated = base_gain[i-1];
  while(i < base_gain.size())
  {
    base_gain[i] = last_value_calculated;
    base_gain_angle[i] = base_gain_angle[i-1]+less_precision_phase_step;
    i++;
  }

}

// Calculate the gain dependeng of the region
float ScanPatternGenerator::radiation_pattern_gain(float phase) 
{ // phase should be in [0,180[
  float G;
  
  if(phase < theta_[0]) 
  {
    G = Gmax_ - 4 * pow(10,-4) * pow(10,Gmax_/10) * pow(phase,2);
  } 

  else if(phase < theta_[1]) 
  {
    G = 0.75*Gmax_ - 7;
  } 
  
  else if(phase < theta_[2]) 
  {
    if(gain_type_ > 1) 
    {
      G = 53 - 0.5*Gmax_ - 25*log10(phase);
    }
  
    else if(gain_type_ == 1) 
    {
      G = 29 - 25*log10(phase); 
    }
  }
 
  else if(phase >= theta_[2]) 
  {
    if(gain_type_ == 1)
      G = -13;
    else if(gain_type_ == 2)
      G = 11 - 0.5 * Gmax_;
    else
      G = 0;
  }
  //LOG(LINFO) << "Gain = " << G;
  return G;
}

///////////////////////////////
//   Fan Circular Pattern    //
///////////////////////////////
float FanCircularScanPatternGenerator::get_gain_sample(int PRI_) 
{
    float linear_gain = pow(10, gain_vector[gain_it]/ 20);

    // Increase the gain_it
    gain_it += PRI_/scale_coeff;

    if (gain_it >= gain_vector.size())
      gain_it = 0;
  
  return linear_gain;
}

float FanCircularScanPatternGenerator::get_radar_angle()
{
  return gain_vector_angle[gain_it];

}

 // Create the gain vector
FanCircularScanPatternGenerator::FanCircularScanPatternGenerator(RadarTx3 *radarsys, float ASP, float Gmax) : ScanPatternGenerator(radarsys, ASP, Gmax) 
{
  max_gain = 0;  

  gain_vector.resize(2*base_gain.size());
  gain_vector_angle.resize(2* base_gain_angle.size());

  // First half of the gain vector
  register unsigned int i = 0;
  while(i < base_gain.size())
  {
    gain_vector[i] = base_gain[i];
    gain_vector_angle[i] = base_gain_angle[i];

    if (gain_vector[i] > max_gain)
      max_gain = gain_vector[i];

    i++;
  }

  // Second half of the gain vector, "flip" the base_gain
  while(i < gain_vector.size())
  {
    gain_vector[i] = gain_vector[2*base_gain.size()-1-i];
    gain_vector_angle[i] = base_gain[i-base_gain.size()]+180;
    i++;
  }
}

///////////////////////////////
//    Fan Sector Pattern     //
///////////////////////////////
float FanSectorScanPatternGenerator::get_gain_sample(int PRI_) 
{
    float linear_gain = pow(10, gain_vector[gain_it]/ 20);

    // Increase the gain_it
    gain_it += PRI_/scale_coeff;

    if (gain_it >= gain_vector.size())
      gain_it = 0;
  
  return linear_gain;
}

float FanSectorScanPatternGenerator::get_radar_angle()
{
  return 0;

}

// Create gain vector
FanSectorScanPatternGenerator::FanSectorScanPatternGenerator(RadarTx3 *radarsys, float ASP, float Gmax, float *angle_range /*, float Rx_angle*/) : ScanPatternGenerator(radarsys, ASP, Gmax) 
{
  // Correct value for center angle
  float center_angle = 	fmod((float) angle_range[0],360.0f);
  if(center_angle < 0)
    center_angle+= 360.0;

  // Correct value for delta
  float delta = 	fmod((float) angle_range[1],360.0f);
  if(delta < 0)
    delta*= -1;

  //LOG(LINFO) << "center\t" << center_angle << "\tdelta\t" << delta << "\tc+d\t" << center_angle + 0.5*delta << "\tc-d\t" << center_angle - 0.5*delta;
  // Calculate the indexes using the angle range 
  int start_angle_index =  (int) (((center_angle-0.5*delta)*base_gain.size())/180);
  int finish_angle_index = (int) (((center_angle+0.5*delta)*base_gain.size())/180);

  //direction_state_ = ((rand() % 2) == 0) ? CLOCKWISE : ANTICLOCKWISE;

  max_gain = 0;  

  gain_vector.resize(2*(finish_angle_index-start_angle_index));
  if (gain_vector.size() == 0)
    gain_vector.resize(1);

  // Negative values
  register int i = start_angle_index;
  //LOG(LINFO) << "Base size " << base_gain.size() << "\tGain Size " << gain_vector.size() << "\tStart "  << start_angle_index << "\tEnd " << finish_angle_index << "\ti" << i << "\ti-start" << (i-start_angle_index);
  while (i < 0)
  {  
    gain_vector[(i-start_angle_index)] = base_gain[-i];
    //LOG(LINFO) << i << "\t" << (i-start_angle_index) << "\t" << gain_vector[(i-start_angle_index)] << "\t" << -i << "\t" << base_gain[-i] << "\t" <<"sub";
    i++;
  }
  // Intermediate values
  while((i < base_gain.size()) && (i <= finish_angle_index)) 
  {
    gain_vector[(i-start_angle_index)] = base_gain[i];
    //LOG(LINFO) << i << "\t" << (i-start_angle_index) << "\t" << gain_vector[(i-start_angle_index)] << "\t" << i << "\t" << base_gain[i] << "\t" <<"mid";
    i++;
  }
  // Higher than base_gain vector size values
  while((i-start_angle_index) < 0.5*gain_vector.size())
  { 
    gain_vector[i-start_angle_index] = base_gain[2*base_gain.size()-1-i];
    //LOG(LINFO) << i << "\t" << (i-start_angle_index) << "\t" << gain_vector[(i-start_angle_index)] << "\t" << (2*base_gain.size()-1-i) << "\t" << base_gain[2*base_gain.size()-1-i] << "\t" <<"sob";
    i++;
  }
  
  // Second half of the gain vector, "flip" the base_gain
  while((i-start_angle_index) < gain_vector.size())
  {
    gain_vector[(i-start_angle_index)] = gain_vector[gain_vector.size()-1-(i-start_angle_index)];
    //LOG(LINFO) << i << "\t" << (i-start_angle_index) << "\t" << gain_vector[(i-start_angle_index)] << "\t" << (gain_vector.size()-1-(i-start_angle_index)) << "\t" << gain_vector[gain_vector.size()-1-(i-start_angle_index)] << "\t" << "over";

    if (gain_vector[i-start_angle_index] > max_gain)
      max_gain = gain_vector[i-start_angle_index];

    i++;
  }
}

///////////////////////////////
//  Pencil Circular Pattern  //
///////////////////////////////
float PencilCircularScanPatternGenerator::get_gain_sample(int PRI_)
{   
    float linear_gain = pow(10, gain_vector[gain_it]/ 20);
 
    // Increase in gain_vector  
    gain_it += PRI_/scale_coeff;

    if (gain_it >= gain_vector.size())
      gain_it = 0;
  
  return linear_gain;
}

float PencilCircularScanPatternGenerator::get_radar_angle()
{
  return 0;

}

// Create the gain vector
PencilCircularScanPatternGenerator::PencilCircularScanPatternGenerator(RadarTx3 *radarsys, float ASP, float Gmax, float elevation_angle) : ScanPatternGenerator(radarsys, ASP, Gmax)
{
  float elevation_degree = elevation_angle;

  // If the elevation angle is negative, correct it
  if (elevation_degree < 0.0)
    elevation_degree *= -1;

  // If the elevation angle is higher than 90, correct it
  if (elevation_degree >= 90.0)
    elevation_degree = 180 - elevation_degree;

  // Discrete representation of the elevation angle
  unsigned int elevation_index = (unsigned int) (elevation_degree*ASP_)/(360.0 * scale_coeff);
  unsigned int radius_limit = (unsigned int) sqrt( (base_gain.size() * base_gain.size()) - ( elevation_index *  elevation_index));
  unsigned int radius;

  max_gain = 0;  
  gain_vector.resize(2*base_gain.size());

  // First half of the gain vector
  register unsigned int i = 0;
  while(i < base_gain.size())
  {
    radius = (unsigned int) sqrt( (i * i) + ( elevation_index *  elevation_index));

    if (radius >= base_gain.size())  
      radius = (unsigned int) sqrt(((2*radius_limit-i)*(2*radius_limit-i))+( elevation_index* elevation_index));    

    gain_vector[i] = base_gain[radius];
    //LOG(LINFO) << i << "\t" << gain_vector[i]  << "\t" << radius << "\t" << base_gain[radius];

    if (gain_vector[i] > max_gain)
      max_gain = gain_vector[i];

    i++;
  }

  while(i < gain_vector.size())
  {
    gain_vector[i] = gain_vector[2*base_gain.size()-1-i];
    //LOG(LINFO) << i << "\t" << gain_vector[i];
    i++;
  }
}

///////////////////////////////
//  Pencil Sector Pattern  //
///////////////////////////////
float PencilSectorScanPatternGenerator::get_gain_sample(int PRI_)
{   
    float linear_gain = pow(10, gain_vector[gain_it]/ 20);
 
    // Increase in gain_vector  
    gain_it += PRI_/scale_coeff;

    if (gain_it >= gain_vector.size())
      gain_it = 0;
  
  return linear_gain;
}

float PencilSectorScanPatternGenerator::get_radar_angle()
{
  return 0;

}

// Create the gain vector
PencilSectorScanPatternGenerator::PencilSectorScanPatternGenerator(RadarTx3 *radarsys, float ASP, float Gmax, float *angle_range, float elevation_angle) : ScanPatternGenerator(radarsys, ASP, Gmax)
{
  float elevation_degree = elevation_angle;

  // If the elevation angle is negative, correct it
  if (elevation_degree < 0.0)
    elevation_degree *= -1;

  // If the elevation angle is higher than 90, correct it
  if (elevation_degree >= 90.0)
    elevation_degree = 180 - elevation_degree;

  // Discrete representation of the elevation angle
  unsigned int elevation_index = (unsigned int) (elevation_degree*ASP_)/(360.0 * scale_coeff);
  unsigned int radius_limit = (unsigned int) sqrt( (base_gain.size() * base_gain.size()) - ( elevation_index *  elevation_index));
  unsigned int radius;

  // Correct value for center angle
  float center_angle = 	fmod((float) angle_range[0],360.0f);
  if(center_angle < 0)
    center_angle+= 360.0;

  // Correct value for delta
  float delta = 	fmod((float) angle_range[1],360.0f);
  if(delta < 0)
    delta*= -1;

  // Calculate the indexes using the angle range 
  int start_angle_index =  (int) (((center_angle-0.5*delta)*base_gain.size())/180);
  int finish_angle_index = (int) (((center_angle+0.5*delta)*base_gain.size())/180);

  //direction_state_ = ((rand() % 2) == 0) ? CLOCKWISE : ANTICLOCKWISE;

  max_gain = 0;  

  gain_vector.resize(2*(finish_angle_index-start_angle_index));
  if (gain_vector.size() == 0)
    gain_vector.resize(1);

  //LOG(LINFO)  <<  start_angle_index << "\t" << finish_angle_index << "\t" << gain_vector.size();
  //LOG(LINFO) << "Base size " << base_gain.size() << "\tGain Size " << gain_vector.size() << "\tStart "  << start_angle_index << "\tEnd " << finish_angle_index;

  // Negative values
  register int i = start_angle_index;
  while (i < 0)
  {  
    radius = (unsigned int) sqrt( (i * i) + ( elevation_index *  elevation_index));

    if (radius >= base_gain.size())  
      radius = (unsigned int) sqrt(((2*radius_limit-1-i)*(2*radius_limit-1-i))+( elevation_index* elevation_index));  

    gain_vector[(i-start_angle_index)] = base_gain[radius];
    //LOG(LINFO) << i << "\t" << (i-start_angle_index) << "\t" << gain_vector[(i-start_angle_index)] << "\t" << -i << "\t" << base_gain[-i] << "sub";
    i++;
  }
  // Intermediate values
  while((i < base_gain.size()) && (i <= finish_angle_index)) 
  {
    radius = (unsigned int) sqrt( (i * i) + ( elevation_index *  elevation_index));

    if (radius >= base_gain.size())  
      radius = (unsigned int) sqrt(((2*radius_limit-1-i)*(2*radius_limit-1-i))+( elevation_index* elevation_index));  

    gain_vector[(i-start_angle_index)] = base_gain[radius];
    //LOG(LINFO) << i << "\t" << (i-start_angle_index) << "\t" << gain_vector[(i-start_angle_index)] << "\t" << i << "\t" << base_gain[i] << "mid";
    i++;
  }
  // Higher than base_gain vector size values
  while((i-start_angle_index) < 0.5*gain_vector.size())
  {
    radius = (unsigned int) sqrt(((2*radius_limit-1-i)*(2*radius_limit-1-i))+( elevation_index* elevation_index));  

    gain_vector[i-start_angle_index] = base_gain[radius];
    //LOG(LINFO) << i << "\t" << (i-start_angle_index) << "\t" << gain_vector[(i-start_angle_index)] << "\t" << (2*base_gain.size()-1-i) << "\t" << base_gain[2*base_gain.size()-1-i] << "sob";
    i++;
  }
  
  // Second half of the gain vector, "flip" the base_gain
  while((i-start_angle_index) < gain_vector.size())
  {
    gain_vector[(i-start_angle_index)] = gain_vector[gain_vector.size()-1-(i-start_angle_index)];
    //LOG(LINFO) << i << "\t" << (i-start_angle_index) << "\t" << gain_vector[(i-start_angle_index)] << "\t" << (gain_vector.size()-1-(i-start_angle_index)) << "\t" << gain_vector[gain_vector.size()-1-(i-start_angle_index)] << "over";

    if (gain_vector[i-start_angle_index] > max_gain)
      max_gain = gain_vector[i-start_angle_index];

    i++;
  }
}

} // namesapce phy
} // namespace iriss
