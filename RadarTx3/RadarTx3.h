

#ifndef RADARTX3COMPONENT_H_
#define RADARTX3COMPONENT_H_

// Iris Include
#include "irisapi/PhyComponent.h"

// Standard Library Include
#include <cmath>
#include <queue> 

// Include from Boost
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/timer.hpp>

// Includes for socket
#include <iostream>
#include <string>
#include <boost/asio.hpp>

#define DEBUG_MODE 1

namespace iris
{
namespace phy
{
typedef std::complex<float> Cplx;
using namespace std;
typedef uint32_t TOA_FORMAT;

enum IPM_TYPE {IPM_PR, IPM_LFM, IPM_PM, IPM_NLFM};

class RadarSigGenerator;
class PulsedRadarGenerator;
class LFMRadarGenerator;
class PMRadarGenerator;

class ScanPatternGenerator;

class SuperCondition 
{
  bool isConditionReady_;
  boost::mutex conditionMutex_;
  boost::condition_variable condition_;

public:
  SuperCondition() : isConditionReady_(false) {}
  inline void wait() 
  {
    boost::unique_lock<boost::mutex> lock(conditionMutex_);
    while(!isConditionReady_) 
      condition_.wait(lock);
  }
  inline void assign(bool val) 
  {
    {
      boost::lock_guard<boost::mutex> lock(conditionMutex_);
      isConditionReady_ = val;
    }
    if(val == true)
      condition_.notify_all();
  }
};


class ControllerBuffer 
{
    SuperCondition var_lock_;

public:
    ControllerBuffer() 
    {
        message_number = 0;
        var_lock_.assign(true);
    }
    
    std::vector<uint8_t> buffer;
    unsigned int message_number;
    uint8_t* begin;
    unsigned int size;

    vector<uint8_t> data(double* time_stamp, float gain) 
    {
        var_lock_.wait();

        buffer.clear();

        // Convert msg_no to uint8_t and concatenate to time stamps
        begin = reinterpret_cast<uint8_t*>(&message_number);
        size = (sizeof(unsigned int)/sizeof(uint8_t));
        buffer.insert(buffer.end(), begin, begin + size);

        begin = reinterpret_cast<uint8_t*>(&time_stamp[0]);
        size = (sizeof(double)/sizeof(uint8_t));
        buffer.insert(buffer.end(), begin, begin + size);

        begin = reinterpret_cast<uint8_t*>(&time_stamp[1]);
        size = (sizeof(double)/sizeof(uint8_t));
        buffer.insert(buffer.end(), begin, begin + size);

        begin = reinterpret_cast<uint8_t*>(&gain);
        size = (sizeof(float)/sizeof(uint8_t));
        buffer.insert(buffer.end(), begin, begin + size);

        // Set a maximun size to deque
        //if ( buffer.size() > 1 )
          //buffer.pop_front();
        
        // Increment messa number
        message_number++;

        var_lock_.assign(true);

        return buffer;        
    }
    //void get_data(vector<uint8_t> &msg_vec);
    //void got_ack(Command &command);
    inline int get_msg_no() 		{ return message_number;	}
    //vector<uint8_t> data()		{ return buffer(); 	}
    string getName() 			{ return "ControllerBuffer";	}
};

class RadarTx3 : public PhyComponent 
{
  friend class RadarSigGenerator;
  friend class PulsedRadarGenerator;
  friend class LFMRadarGenerator;
  friend class PMRadarGenerator;
  friend class ScanPatternGenerator;
  
  double num_samp_gen_;
  RadarSigGenerator *radar_ptr;
  
  // Antenna Radiation and Scan Pattern
  ScanPatternGenerator *esp_gen_; // Creates effective scan pattern

  // XML Input registered parameters
  float fsampling_;
  float param_PTx_;
  string ipm_class_;
  float param_PW_;
  float param_PRI_;
  float param_freq_excursion_;
  int param_barker_;
  float param_ASP_;
  string param_scan_type_;
  float param_Gmax_;
  float param_angle_range_[2];
  float param_rx_angle_;
  float param_elevation_;
  
  double clockupdate_;
  int sleep_time_;
  int ack_tstamp_;
  int n_times_;
  float old_angle;
  SuperCondition sleep_lock_;
  SuperCondition sleep_lock2_;

  ControllerBuffer rx_controller;
  boost::scoped_ptr< boost::thread > comm_thread_;
  
public:
  RadarTx3(std::string name);
  ~RadarTx3();
  virtual void calculateOutputTypes(std::map<std::string, int>& inputTypes, std::map<std::string, int>& outputTypes);
  virtual void registerPorts();
  virtual void initialize();
  virtual void process();
  virtual void parameterHasChanged(std::string name);
  void comm_thread();

  void generate_radiation_pattern(float Gmax);

private:
  void destroy();

};

class RadarSigGenerator 
{
  IPM_TYPE IPM_;
protected:

public:
  RadarTx3 &radarsys_;
  float PAmp_;
  TOA_FORMAT PW_;
  TOA_FORMAT PRI_;
  TOA_FORMAT mode_transition_delay_;
  vector<Cplx> pulse_waveform_;
  TOA_FORMAT gen_clock_;
  TOA_FORMAT temp_PRI_;
  RadarSigGenerator(RadarTx3 *radarsys, IPM_TYPE ipm);
  virtual void generate_pulse() = 0;
  virtual inline Cplx get_waveform_sample();
  void advance_clock(TOA_FORMAT jump) { gen_clock_ = (gen_clock_ + jump) % temp_PRI_;}
  inline TOA_FORMAT samples_until_pulse_start() { return (PRI_ - gen_clock_); }
  inline TOA_FORMAT get_pulse_duration() { return PW_; }
  inline TOA_FORMAT get_period_duration() { return PRI_; }
  void set_PAmp(float val);

  inline bool is_rx_mode() 
  {
    return (gen_clock_ > (PW_ + mode_transition_delay_) && gen_clock_ < (temp_PRI_ - mode_transition_delay_));
  }

  inline string getName() {return "RadarSigGenerator";}
  string print() const;
};


///////////////////////////////
//  Intrapulse Modulations   //
///////////////////////////////
class PulsedRadarGenerator : public RadarSigGenerator 
{
public:
  PulsedRadarGenerator(RadarTx3 *radarsys);
  void generate_pulse();
};

class LFMRadarGenerator : public RadarSigGenerator 
{
  float freq_excursion_;
public:
  LFMRadarGenerator(RadarTx3 *radarsys);//, float freq_excursion);
  void generate_pulse();
};

class PMRadarGenerator : public RadarSigGenerator 
{  
  const int *barker_mask;
  int barker_order_;

public:
  PMRadarGenerator(RadarTx3 *radarsys);
  void generate_pulse();
};


///////////////////////////////
//  Scan Pattern Generation  //
///////////////////////////////
class ScanPatternGenerator 
{
  protected:
    RadarTx3 &radarsys_;

    // Phase step and delimiting angle for gain the regions
    double phase_step_;
    vector<float> theta_;
    uint8_t gain_type_;

  public:
    float Gmax_;
    float Gmin_;
    inline string getName() {return "ScanPatternGenerator";}
    unsigned int gain_it;
    ScanPatternGenerator(RadarTx3 *radarsys, float ASP, float Gmax);

    virtual float get_gain_sample(int PRI_input) = 0;
    virtual float get_radar_angle() = 0;

    float radiation_pattern_gain(float phase);

    TOA_FORMAT ASP_;
    vector<float> base_gain;
    vector<float> base_gain_angle;

    float scale_coeff;
    

};

///////////////////////////////
//   Fan Circular Pattern    //
///////////////////////////////
class FanCircularScanPatternGenerator : public ScanPatternGenerator 
{
  vector<float> gain_vector;
  vector<float> gain_vector_angle;
  float max_gain;

  public:
    FanCircularScanPatternGenerator(RadarTx3 *radarsys, float ASP, float Gmax);
    float get_gain_sample(int PRI_input);
    float get_radar_angle();
};

///////////////////////////////
//     Fan Sector Pattern    //
///////////////////////////////
class FanSectorScanPatternGenerator : public ScanPatternGenerator 
{
  vector<float> gain_vector;
  float max_gain;

  //enum SCAN_DIRECTION {CLOCKWISE, ANTICLOCKWISE};
  //SCAN_DIRECTION direction_state_;
  
  public:
    FanSectorScanPatternGenerator(RadarTx3 *radarsys, float ASP, float Gmax, float *angle_range); //, float Rx_angle);
    float get_gain_sample(int PRI_input);
    float get_radar_angle();
};

///////////////////////////////
//  Pencil Circular Pattern  //
///////////////////////////////
class PencilCircularScanPatternGenerator : public ScanPatternGenerator
{
  vector<float> gain_vector;
  float max_gain;

  public:
    PencilCircularScanPatternGenerator(RadarTx3 *radarsys, float ASP, float Gmax, float elevation_angle);
    float get_gain_sample(int PRI_input);
    float get_radar_angle();
};

///////////////////////////////
//   Pencil Sector Pattern   //
///////////////////////////////
class PencilSectorScanPatternGenerator : public ScanPatternGenerator 
{
  vector<float> gain_vector;
  float max_gain;

  //enum SCAN_DIRECTION {CLOCKWISE, ANTICLOCKWISE};
  //SCAN_DIRECTION direction_state_;
  
  public:
    PencilSectorScanPatternGenerator(RadarTx3 *radarsys, float ASP, float Gmax, float *angle_range, float elevation_angle);
    float get_gain_sample(int PRI_input);
    float get_radar_angle();
};

} // namespace phy
} // namespace iris

#endif // PHY_EXAMPLECOMPONENT_H_
