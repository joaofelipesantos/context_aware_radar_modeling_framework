
#include "RadarRx3.h"
#include "irisapi/LibraryDefs.h"
#include "irisapi/Version.h"
#include <unistd.h>


using namespace std;

namespace iris
{
namespace phy
{

// Wxport library symbols
IRIS_COMPONENT_EXPORTS(PhyComponent, RadarRx3);

// Register input component parameters
RadarRx3::RadarRx3(std::string name) : PhyComponent(name, "radarrx3", "Receives radar signals", "Joao F. Santos", "0.1") 
{
    registerParameter("controller_data", "Tx finished transmission", "0", true, new_time_stamp_);
    registerParameter("inst_gain", "Tx instantaneous gain", "0", true, inst_gain_);
    registerParameter("pulse_size", "Pulse size", "0", true, pulse_size_);
    registerParameter("max_gain", "Maximum gain", "0", true, max_gain_);
    registerParameter("min_gain", "Minimun gain", "1", true, min_gain_);
}

// Register ports
void RadarRx3::registerPorts() 
{
    registerInputPort("input1", TypeInfo< Cplx >::identifier);
}

// Calculate output types
void RadarRx3::calculateOutputTypes(std::map<std::string,int>& inputTypes, std::map<std::string,int>& outputTypes) 
{
}

// Change parameters dynamically and restart radar system
void RadarRx3::parameterHasChanged(std::string name)
{
  if(name == "controller_data")
  {
    time_stamps_stack.push_back(new_time_stamp_);
  }

  if(name == "inst_gain")
    inst_gain_stack.push_back(inst_gain_);
}

// Initialize the Radar Receiver
void RadarRx3::initialize() 
{
  // File declarations
  time_file.open ("time_delay.txt", ios::out | ios::trunc);
  power_file.open ("power_measure.txt", ios::out | ios::trunc);
  gain_file.open ("gain_test.txt", ios::out | ios::trunc);

  // First value assignemnts
  number_of_pulses = 0;
  power_o = 0;
  max_power_value = 0;
  min_power_value = 1;
  ta_ = boost::posix_time::microsec_clock::local_time();
}

/* ************************************************************************** */
/* ******************************** Proccess  ******************************* */
/* ************************************************************************** */
void RadarRx3::process() 
{	
    // Initialize the inout DataSet
    DataSet<Cplx>* readDataSet = NULL;
    getInputDataSet("input1", readDataSet);

    // Calculate DataSet size
    unsigned int data_set_size = std::distance(readDataSet->data.begin(),readDataSet->data.end());

    // Get DataSet data
    read_tstamp_ = readDataSet->timeStamp;
    read_fsamp_ = readDataSet->sampleRate;
    time_step = 1.0 / read_fsamp_;
    double read_tstamp_begin = read_tstamp_ - (time_step*data_set_size);

    // Wait until the RadarTX start working
    if ((read_tstamp_ >= time_stamps_stack.front()) && (!time_stamps_stack.empty()))
    { 
      // Correct the time difference betwee TX and RX iterating the DataSet using an index 
      index =  read_fsamp_ * (read_tstamp_begin - time_stamps_stack.front());
      number_of_pulses++;

      // Variables to output the levels of RX signal and noise floor
      signal = 0;
      noise_floor = 0;
      artificial_noise = 0;

      rx_cntr = 0;
      sg_cntr = 0;

      // Iterate through the DataSet
      vector<Cplx>::iterator input_dataset_iterator = readDataSet->data.begin();
      do
      {   
        // Read value from te DataSet
        Cplx received_value = *input_dataset_iterator;
        received = std::norm(received_value); 

        //if (received != 0)
        {
        if (received > max_power_value)
          max_power_value = received;

        //LOG(LINFO) << max_power_value << "\t" << min_power_value << "\t" << received; 
        power_file << received << "\t" <<index << "\t" << pulse_size_ << "\n";
        
        // Signal and noise separation
        {

          /* ************************************************************************** */
          /* ************************ Noise Floor Before Pulse ************************ */
          /* ************************************************************************** */
          if (index < pulse_size_)
          {
            if (received < 0.5*min_power_value )
            {
              noise_floor +=received;
              real = normal_sample(index,power_o);
              imag = normal_sample(-index,power_o);
            
              Cplx noise(real,imag);
              artificial_noise += std::norm(noise+(received_value)*std::sqrt(inst_gain_stack.front()));
              rx_cntr++;
            }
          }

          /* ************************************************************************** */
          /* ************************ Signal Level Inside Pulse *********************** */
          /* ************************************************************************** */
          else if ((index >= (pulse_size_+4)) && (index <= (2*pulse_size_+4)))
          { 
            if (received < min_power_value)
              min_power_value = received;

            signal += received;
            sg_cntr++;  
          }

          /* ************************************************************************** */
          /* ************************ Noise Floor After Pulse  ************************ */
          /* ************************************************************************** */
          else if (index > (2*pulse_size_+8))
          {
            if (received < 0.5*min_power_value )
            {
              noise_floor +=received;
              real = normal_sample(index,power_o);
              imag = normal_sample(-index,power_o);
            
              Cplx noise(real,imag);
              float temp = (inst_gain_stack[0]);
              artificial_noise += std::norm(noise+(received_value)*std::sqrt(inst_gain_stack.front()));
              rx_cntr++;
            }   
          }

        }
        // Increment the number of received samples
        index++;
        }
        // Increment the DataSet iterator
        input_dataset_iterator++;

      }while( (input_dataset_iterator != readDataSet->data.end()));

      // SNIR part
      {
        //n_0 = noise_floor/(index-(pulse_size_+32));
        n_0 = noise_floor/rx_cntr;
        //gain_t = signal/(pulse_size_+1);         
        gain_t = signal/sg_cntr;         

        power_o = n_0*(max_gain_ - inst_gain_stack.front());
        artificial_noise_power = artificial_noise/rx_cntr;

      }

      /* ************************************************************************** */
      /* ************************** Output Data to Files  ************************* */
      /* ************************************************************************** */
      {
        //time_file << signal << "\t" << signal/pulse_size_ << "\t" << noise_floor << "\t"<< noise_floor/(number_of_samples-pulse_size_) << "\n";

        gain_file << max_gain_ << "\t" << inst_gain_stack.front() << "\t" << n_0 << "\t" << power_o << "\t" << artificial_noise_power << "\n";
      }

      time_stamps_stack.pop_front();
      inst_gain_stack.pop_front();

    }
    // Release the DataSet
    releaseInputDataSet("input1", readDataSet);

 
}

/* ************************************************************************** */
/* ************************ Generates Noise Samples  ************************ */
/* ************************************************************************** */
double RadarRx3::normal_sample(int extra_seed, double variance)
{
  // Distribution declaration
  normal_distribution<double> noise_part(0.0,sqrt(variance/4));

  // Seed generation 
  mt19937 random_seed(extra_seed-std::time(0));               

  // Random number generation
  variate_generator<mt19937&,normal_distribution<double> > noise_distribution(random_seed,noise_part);

  return noise_distribution();
}


} // namesapce phy
} // namespace irisinput_dataset_iterator += index;
