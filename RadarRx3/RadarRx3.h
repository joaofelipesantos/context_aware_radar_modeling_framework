

#ifndef RADARRX3COMPONENT_H_
#define RADARRX3COMPONENT_H_

// Include Iris Component
#include "irisapi/PhyComponent.h"

// Include from STD
#include <fstream>
#include <complex> 
#include <cmath>

// Include from Boost
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/timer.hpp>
#include <boost/random.hpp> 
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>


#define DEBUG_MODE 1

namespace iris
{
namespace phy
{
typedef std::complex<float> Cplx;
using namespace std;
using namespace boost;
typedef uint32_t TOA_FORMAT;

class RadarRx3 : public PhyComponent 
{
  // DataSet data info variables
  list<float> gain_tstamps_;
  double read_tstamp_;
  double read_fsamp_;
  double time_step;

  boost::posix_time::ptime ta_;

  // Time stamps
  std::deque<double> time_stamps_stack;
  double new_time_stamp_;

  // Intantaneous gain
  std::deque<float> inst_gain_stack;
  float inst_gain_;
  float max_gain_;
  float min_gain_;

  // Received value
  double received;

  // Pulse Parameters
  unsigned int pulse_size_;
  double signal;
  double gain_t;

  // Noise Parameters
  double noise_floor;
  double n_0;

  // Complementary gain value
  double power_o;
  double artificial_noise_power;

  // Iteration
  unsigned long int number_of_pulses;
  int  index;

  // File declarations for debugging
  std::ofstream time_file;
  std::ofstream power_file;
  std::ofstream gain_file;

  // INR variables  
  double max_power_value;
  double min_power_value;
  int rx_cntr;
  int sg_cntr;
  double artificial_noise;
  double real;
  double imag;

  virtual double normal_sample(int extra_seed, double variance);


public:
  RadarRx3(std::string name);
  virtual void calculateOutputTypes(std::map<std::string, int>& inputTypes, std::map<std::string, int>& outputTypes);
  virtual void registerPorts();
  virtual void initialize();
  virtual void process();
  virtual void parameterHasChanged(std::string name);

  void tx_statistics(Cplx &samp, float &gain);
  void rx_statistics(Cplx &samp, float &gain);
  void transition_statistics(Cplx &samp, float &gain);

private:
};

} // namespace phy
} // namespace iris

#endif
